name := "bullet"

version := "1.0"

scalaVersion := "2.10.3"

scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked")

resolvers ++= Seq("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/")

libraryDependencies ++= Seq(
  "com.typesafe.akka" % "akka-actor_2.10" % "2.2.3",
  "org.slf4j" % "slf4j-api" % "1.7.5",
  "org.eintr.loglady" % "loglady_2.10" % "1.1.0",
  "ch.qos.logback" % "logback-classic" % "1.0.13",
  "ch.qos.logback" % "logback-core" % "1.0.13",
  "com.github.nscala-time" % "nscala-time_2.10" % "0.8.0"
)