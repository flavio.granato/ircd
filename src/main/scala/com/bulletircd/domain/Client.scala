package com.bulletircd.domain

import akka.actor.IO.SocketHandle

class Client(_nick: String, hostname: String = "", servername: String = "", _realname: String = "") {
  val mode = Seq.empty
  def realname = _realname
  def nick = _nick
  var handle: SocketHandle

  def setMode(_mode: String): Unit = {
    mode + _mode
  }


}
