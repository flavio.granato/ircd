package com.bulletircd.domain

import akka.actor.{IOManager, IO, Actor}
import org.eintr.loglady.Logging
import com.bulletircd.infra.Parse
import java.net.InetSocketAddress
import akka.util.ByteString
import com.github.nscala_time.time.Imports._
import akka.actor.IO.ReadHandle

class Server(port: Int) extends Actor with Logging {
  val uptime: Long = DateTime.now.millis
  val version: String = "0.0.1"
  val serverName: String = "irc.bulletircd.com"
  var nicks: Seq[String] = Seq.empty
  var clients: Seq[Client] = Seq.empty
  val parser = new Parse

  override def preStart() {
    IOManager(context.system).listen(new InetSocketAddress(port))
  }

  def handleNick(handle: ReadHandle, msg: String): Unit = {
    val nick = msg split " "

    if (nicks.contains(nick(1))) {
      def responseId: Int = response.withName("ERR_NICKNAMEINUSE").id
      def out = s":$serverName $responseId * ${nick(0)} :Nickname is already in use.\r\n"
      handle.asWritable.write(ByteString.fromString(out))
    } else {
      nicks :+= nick(1)
    }
  }

  def handleUser(command: String): Client = {
    val realname = command split ":"
    val user = command split " "

    new Client(user(1), user(3), "serverName", realname(1))
  }

  def sendWelcomeMessage(handle: ReadHandle, client: Client) = {
    val responseId: Int = response.withName("RPL_WELCOME").id
    val out = s":$serverName $responseId * ${client.nick} :Welcome to this IRC server. :${client.realname}\r\n"
    handle.asWritable.write(ByteString.fromString(out))
  }

  def sendYourHost(handle: ReadHandle, client: Client) = {
    val responseId: Int = response.withName("RPL_YOURHOST").id
    val out = s":$serverName $responseId ${client.nick} Auth :*** Looking up your hostname ***\r\n"
    handle.asWritable.write(ByteString.fromString(out))
  }

  def sendServerInfo(handle: ReadHandle, client: Client) = {
    val responseId: Int = response.withName("RPL_MYINFO").id
    val out = s":$serverName $responseId ${client.nick} $serverName $version abcd abc\r\n"
    handle.asWritable.write(ByteString.fromString(out))
  }

  def sendCreatedMessage(handle: ReadHandle, client: Client) = {
    val responseId: Int = response.withName("RPL_CREATED").id
    val out = s":$serverName $responseId ${client.nick} This server was created $uptime \r\n"
    handle.asWritable.write(ByteString.fromString(out))
  }

  def sendMOTD(handle: ReadHandle, client: Client) = {
    handle.asWritable.write(ByteString.fromString(":localhost " + response.withName("RPL_MOTDSTART").id
      + " " + client.nick + " - Message of the Day\r\n"))

    for (line <- scala.io.Source.fromFile("/home/flavio/motd.txt", "utf-8").getLines) {
      handle.asWritable.write(ByteString.fromString(":localhost " + response.withName("RPL_MOTD").id
        + " " + client.nick + " " + line + "\r\n"))
    }

    handle.asWritable.write(ByteString.fromString(":localhost " + response.withName("RPL_ENDOFMOTD").id
      + " " + client.nick + " - End of /MOTD command.\r\n"))
  }

  def receive = {
    case IO.NewClient(server) =>
      server.accept()
    case IO.Read(handle, bytes) =>
      val command = bytes.utf8String.trim
      val commands: Array[String] = command split "\r\n"

      commands foreach {
        case command if command.startsWith("NICK") =>
          handleNick(handle, command)
        case command if command.startsWith("USER") =>
          val newClient: Client = handleUser(command)
          newClient.handle = handle.asSocket
          clients :+ newClient

          if (newClient != null) {
            sendWelcomeMessage(handle, newClient)
            sendYourHost(handle, newClient)
            sendCreatedMessage(handle, newClient)
            sendServerInfo(handle, newClient)
            sendMOTD(handle, newClient)
          }
        case command =>
          println(command)
          handle.asWritable.write(ByteString.fromString(":localhost " + response.withName("ERR_UNKNOWNCOMMAND").id
            + " " + command + " :Unknown command\r\n"))
    }
  }
}
