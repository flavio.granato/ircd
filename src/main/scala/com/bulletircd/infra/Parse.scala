package com.bulletircd.infra

import org.eintr.loglady.Logging
import com.bulletircd.domain.Command

class Parse extends Logging {
  def parse(message: String): Seq[Command] = {
    val values: Array[String] = message split "\r\n"
    var result: Seq[Command] = Seq.empty

      values foreach { value =>
        val command :Array[String] = value split " "
        result :+= new Command(command.head, command(1))
      }
    result
  }
}
