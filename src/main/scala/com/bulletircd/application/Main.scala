package com.bulletircd.application

import akka.actor._

import com.bulletircd.domain.Server

object Main extends App {
  val port = 6667
  ActorSystem().actorOf(Props(new Server(port)))
}
